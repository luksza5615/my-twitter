package pl.szata.myfacebook.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.szata.myfacebook.entity.Post;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PostRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PostRepository postRepository;

    @Test
    public void whenFindById_thenReturnPost() {
        //given
        Post post = new Post();
        entityManager.persist(post);
        entityManager.flush();

        //when
        Optional<Post> foundPost = postRepository.findPostById(post.getId());

        //then
        assertThat(post.getId().equals(foundPost.get().getId()));
    }



}
