drop database if exists `twitter2`;
create database `twitter2`;
use `twitter2`;
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `user_details`;
create table `user_details`
(id int(11) not null auto_increment,
name varchar(50) not null,
surname varchar(50) not null,
email varchar(50) not null,
birth_date date not null,
join_date date not null,
primary key(`id`)
);

DROP TABLE IF EXISTS `user`;
create table `user`
(id int(11) not null auto_increment,
username varchar(50) not null,
password varchar(50) not null,

user_details_id int(50),
lock_date timestamp null,
unlock_date timestamp null,
primary key(`id`),
foreign key (`user_details_id`) references `user_details`(`id`)

);

DROP TABLE IF EXISTS `user_role`;
create table `user_role`
(id int(11) not null auto_increment,
role varchar(50),
description varchar(50),
primary key(`id`)
);

DROP TABLE IF EXISTS `user_user_role`;
create table `user_user_role`
(id int(11) not null auto_increment,
user_id int(11),
role_id int(11),
foreign key (`user_id`) references `user`(`id`),
foreign key (`role_id`) references `user_role`(`id`),
primary key(`id`)
);


/*
drop table if exists `roles`;
CREATE TABLE `roles` (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(50) NOT NULL,
  role varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY uni_username_role (`role`, `username`),
  KEY fk_username_idx (username),
  FOREIGN KEY (`username`) REFERENCES `user`(`username`));
*/

DROP TABLE IF EXISTS `post`;
create table `post`
(id int(11) not null auto_increment,
create_date timestamp null,
text varchar(300) not null,
modify_date timestamp null,
user_id int(5),
deleted boolean,
delete_date timestamp null,
primary key(`id`),
foreign key(`user_id`) references `user`(`id`)
);


DROP TABLE IF EXISTS `comment`;
create table `comment`
(id int(11) not null auto_increment,
create_date timestamp not null,
text varchar(300) not null,
modify_date timestamp null,
user_id int(5),
-- deleted boolean,
delete_date timestamp null,
post_id int(5),
primary key(`id`),
foreign key(`user_id`) references `user`(`id`),
foreign key(`post_id`) references `post`(`id`)
);

SET FOREIGN_KEY_CHECKS=1;

