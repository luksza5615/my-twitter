# insert into post (body) values ('Przykladowy post1');
# insert into post (body) values ('Przykladowy post2');
# insert into comment (text) values ('Komentarz do postu 1');
# insert into comment (text) values ('Komentarz do postu 1');
# insert into comment (text) values ('Komentarz do postu 2');
# insert into comment (text) values ('Komentarz do postu 2');
SET FOREIGN_KEY_CHECKS=0;
# delete from user;
delete from user_role;
# delete from user;
# delete from user_details;
delete from post;
delete from comment;
insert into user_role (id, role, description) values(1, 'ROLE_USER', 'user role');
insert into user_role(id, role, description) values (2, 'ROLE_ADMIN', 'admin role');
# insert into user_details (id, name, email, birth_date, join_date) values (1, 'Name', 'name_surname@gmail.com', '2019-01-01 19:45:12', '2019-01-01 19:45:12');
# insert into user (id, username, password, user_details_id, lock_date, unlock_date) values (1, 'test', 'pass', 1, '2019-01-01 19:45:12', '2019-01-01 19:45:12');
# insert into user (username, password) values ('test22', 'pass');
insert into post (id, create_date, text, modify_date, user_id, delete_date) values (1, '2019-01-01 19:45:12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
                   turpis sem, dictum id bibendum eget, malesuada ut massa. Ut scel
                   erisque nulla sed luctus dapibus. Nulla sit amet mi vitae purus sol
                   licitudin venenatis. Praesent et sem urna. Integer vitae lectus nis
                   l. Fusce sapien ante, tristique efficitur lorem et, laoreet ornare lib
                   ero. Nam fringilla leo orci. Vivamus semper quam nunc, sed ornare magna dignissim sed. Etiam interdum justo quis risus
                   efficitur dictum. Nunc ut pulvinar quam. N
                   unc mollis, est a dapibus dignissim, eros elit tempor diam, eu tempus quam felis eu velit.', null, 1, null);
insert into post (id, create_date, text, modify_date, user_id, delete_date) values (2, '2019-01-01 19:45:12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
                   turpis sem, dictum id bibendum eget, malesuada ut massa. Ut scel
                   erisque nulla sed luctus dapibus. Nulla sit amet mi vitae purus sol
                   licitudin venenatis. Praesent et sem urna. Integer vitae lectus nis
                   l. Fusce sapien ante, tristique efficitur lorem et, laoreet ornare lib
                   ero. Nam fringilla leo orci. Vivamus semper quam nunc, sed ornare magna dignissim sed. Etiam interdum justo quis risus
                   efficitur dictum. Nunc ut pulvinar quam. N
                   unc mollis, est a dapibus dignissim, eros elit tempor diam, eu tempus quam felis eu velit.', null, 1, null);
insert into post (id, create_date, text, modify_date, user_id, delete_date) values (3, '2019-01-01 19:45:12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
                   turpis sem, dictum id bibendum eget, malesuada ut massa. Ut scel
                   erisque nulla sed luctus dapibus. Nulla sit amet mi vitae purus sol
                   licitudin venenatis. Praesent et sem urna. Integer vitae lectus nis
                   l. Fusce sapien ante, tristique efficitur lorem et, laoreet ornare lib
                   ero. Nam fringilla leo orci. Vivamus semper quam nunc, sed ornare magna dignissim sed. Etiam interdum justo quis risus
                   efficitur dictum. Nunc ut pulvinar quam. N
                   unc mollis, est a dapibus dignissim, eros elit tempor diam, eu tempus quam felis eu velit.', null, 1, null);
insert into comment (id, create_date, text, modify_date, user_id, delete_date, post_id) values (1, '2019-01-01 19:45:12', 'Hello this is a test comment and this comment is particularly very long and it goes on and on and on.', null, 1, null, 1);
insert into comment (id, create_date, text, modify_date, user_id, delete_date, post_id) values (2, '2019-01-01 19:45:12', 'Hello this is a test comment and this comment is particularly very long and it goes on and on and on.', null, 1, null, 2);
insert into comment (id, create_date, text, modify_date, user_id, delete_date, post_id) values (3, '2019-01-01 19:45:12', 'Hello this is a test comment and this comment is particularly very long and it goes on and on and on.', null, 1, null, 2);
insert into comment (id, create_date, text, modify_date, user_id, delete_date, post_id) values (4, '2019-01-01 19:45:12', 'Hello this is a test comment and this comment is particularly very long and it goes on and on and on.', null, 1, null, 2);
SET FOREIGN_KEY_CHECKS=1;