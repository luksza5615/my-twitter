package pl.szata.myfacebook.util;

import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.szata.myfacebook.entity.User;

@Component
public class UserChecker {

    @Bean
    public UserChecker createUserChecker() {
        return new UserChecker();
    }

    public String checkCurrentUserByUsername() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    public boolean isLoggedUserAdmin(User user) {
        return SecurityContextHolder.getContext().getAuthentication()
                .getAuthorities().stream()
                .map(a -> ((GrantedAuthority) a).toString()).anyMatch(a -> a.equals("ROLE_ADMIN"));
    }
}
