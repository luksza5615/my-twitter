package pl.szata.myfacebook.util;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.szata.myfacebook.entity.User;

@Component
public class ThymeleafUtils {

    public static String reverseString(String input) {
        return "blabla";
    }

    public boolean isAuthorOrAdmin(User user) {
        boolean isAuthor = SecurityContextHolder.getContext().getAuthentication().getName().equals(user.getUsername());

        boolean isAdmin = SecurityContextHolder.getContext().getAuthentication()
                .getAuthorities().stream()
                .map(a -> ((GrantedAuthority) a).toString()).anyMatch(a -> a.equals("ROLE_ADMIN"));

        return isAuthor || isAdmin;
    }
}
