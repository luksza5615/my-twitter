package pl.szata.myfacebook.entity;

import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "create_date")
    private Timestamp createDate;

    private String text;

    @Column(name = "modify_date")
    private Timestamp modifyDate;

    @Column(name = "delete_date")
    private Timestamp deleteDate;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "post", cascade=CascadeType.ALL)
    @Where(clause="delete_date is null")
    private List<Comment> comments;

}
