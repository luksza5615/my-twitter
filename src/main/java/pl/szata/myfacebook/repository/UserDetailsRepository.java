package pl.szata.myfacebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.szata.myfacebook.entity.UserDetails;

public interface UserDetailsRepository extends JpaRepository<UserDetails, Long> {
}
