package pl.szata.myfacebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.szata.myfacebook.entity.Post;

import java.util.List;
import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, Long> {
    Optional<Post> findPostById(Long id);

    List<Post> findByDeleteDateNull();

//    other method to filter commments and posts
//    @Query("select distinct p from Post p join p.comments c where p.deleteDate is null and c.deleteDate is null ")
//    List<Post> getNotDeletedPostsWithComments();

}
