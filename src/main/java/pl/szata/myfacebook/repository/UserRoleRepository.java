package pl.szata.myfacebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.szata.myfacebook.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    UserRole findByRole(String role);

}
