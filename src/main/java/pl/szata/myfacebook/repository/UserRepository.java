package pl.szata.myfacebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.szata.myfacebook.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    Optional<User> findById(Long id);

    @Query("SELECT u FROM User u WHERE u.unlockDate is not null AND u.unlockDate <= CURRENT_TIMESTAMP")
    List<User> findAllToUnlock();

}
