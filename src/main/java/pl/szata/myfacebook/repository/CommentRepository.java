package pl.szata.myfacebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.szata.myfacebook.entity.Comment;

import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    Optional<Comment> findCommentById(Long id);

}
