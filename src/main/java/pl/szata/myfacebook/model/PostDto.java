package pl.szata.myfacebook.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.szata.myfacebook.entity.Comment;
import pl.szata.myfacebook.entity.User;

import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
public class PostDto {

    private Long id;

    private String text;

    private Timestamp createDate;

    private List<Comment> comments;

    private User user;

}
