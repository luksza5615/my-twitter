package pl.szata.myfacebook.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.szata.myfacebook.entity.User;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
public class CommentDto {

    private Long id;

    private String text;

    private Timestamp createDate;

    private Long postId;

    private User user;

}
