package pl.szata.myfacebook.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.szata.myfacebook.entity.UserRole;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class UserDto {

    private Long id;

    private String username;

    private String password;

    private Timestamp lockDate;

    private Timestamp unlockDate;

    private Set<UserRole> roles = new HashSet<>();

    private UserDetailsDto userDetails;
}

