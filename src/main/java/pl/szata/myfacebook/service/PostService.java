package pl.szata.myfacebook.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.szata.myfacebook.entity.Comment;
import pl.szata.myfacebook.entity.Post;
import pl.szata.myfacebook.entity.User;
import pl.szata.myfacebook.model.PostDto;
import pl.szata.myfacebook.repository.PostRepository;
import pl.szata.myfacebook.repository.UserRepository;
import pl.szata.myfacebook.util.LocalDateTimeAttributeConverter;
import pl.szata.myfacebook.util.UserChecker;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PostService {
    private ModelMapper modelMapper;
    private PostRepository postRepository;
    private LocalDateTimeAttributeConverter localDateTimeAttributeConverter;
    private UserChecker userChecker;
    private UserRepository userRepository;

    @Autowired
    public PostService(ModelMapper modelMapper,
                       PostRepository postRepository,
                       LocalDateTimeAttributeConverter localDateTimeAttributeConverter,
                       UserChecker userChecker,
                       UserRepository userRepository) {
        this.modelMapper = modelMapper;
        this.postRepository = postRepository;
        this.localDateTimeAttributeConverter = localDateTimeAttributeConverter;
        this.userChecker = userChecker;
        this.userRepository = userRepository;
    }

    public List<PostDto> getAllPosts() {
        List<Post> posts = postRepository.findByDeleteDateNull();

        return posts.stream()
                .map(p -> modelMapper.map(p, PostDto.class))
                .peek(p -> {
                    List<Comment> sortedComments = p.getComments().stream()
                            .sorted(new Comparator<Comment>() {
                                @Override
                                public int compare(Comment comment1, Comment comment2) {
                                    return -(comment1.getCreateDate().compareTo(comment2.getCreateDate()));
                                }
                            }).collect(Collectors.toList());

                    p.setComments(sortedComments);

                })
                .sorted(Comparator.comparing(PostDto::getId).reversed())
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public void addNewPost(PostDto postDto) {
        User user = userRepository.findByUsername(userChecker.checkCurrentUserByUsername());
        postDto.setCreateDate(localDateTimeAttributeConverter.convertToDatabaseColumn(LocalDateTime.now()));
        postDto.setUser(user);
        Post post = modelMapper.map(postDto, Post.class);
        postRepository.save(post);
    }

    @Transactional
    public void deletePost(Long id) {
        User loggedUser = userRepository.findByUsername(userChecker.checkCurrentUserByUsername());

        Post post = postRepository.findPostById(id)
                .orElseThrow(() -> new RuntimeException("Post not found"));

        boolean isLoggedUserAdmin = userChecker.isLoggedUserAdmin(loggedUser);

        if ((!post.getUser().getId().equals(loggedUser.getId())) && !isLoggedUserAdmin) {
            throw new RuntimeException("Deleting not yours post not possible");
        }

        post.setDeleteDate(localDateTimeAttributeConverter.convertToDatabaseColumn(LocalDateTime.now()));

    }

    public void modifyPost(PostDto postDto) {
        User loggedUser = userRepository.findByUsername(userChecker.checkCurrentUserByUsername());

        Post post = postRepository.findPostById(postDto.getId())
                .orElseThrow(() -> new RuntimeException("Post not found"));

        boolean isLoggedUserAdmin = userChecker.isLoggedUserAdmin(loggedUser);

        if ((!post.getUser().getId().equals(loggedUser.getId())) && !isLoggedUserAdmin) {
            throw new RuntimeException("Modifying not yours post not possible");
        }

        post.setText(postDto.getText());
        post.setModifyDate(localDateTimeAttributeConverter.convertToDatabaseColumn(LocalDateTime.now()));
        postRepository.save(post);
    }


}
