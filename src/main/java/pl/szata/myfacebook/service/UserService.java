package pl.szata.myfacebook.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.szata.myfacebook.entity.User;
import pl.szata.myfacebook.entity.UserRole;
import pl.szata.myfacebook.model.UserDto;
import pl.szata.myfacebook.repository.UserRepository;
import pl.szata.myfacebook.repository.UserRoleRepository;
import pl.szata.myfacebook.util.LocalDateTimeAttributeConverter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private static final String DEFAULT_ROLE = "ROLE_USER";
    private static final long ACCOUNT_BLOCK_TIME_IN_MINUTES = 10;
    private UserRepository userRepository;
    private UserRoleRepository userRoleRepository;
    private ModelMapper modelMapper;
    private LocalDateTimeAttributeConverter localDateTimeAttributeConverter;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository,
                       UserRoleRepository userRoleRepository,
                       ModelMapper modelMapper,
                       LocalDateTimeAttributeConverter localDateTimeAttributeConverter,
                       PasswordEncoder passwordEncoder
    ) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.modelMapper = modelMapper;
        this.localDateTimeAttributeConverter = localDateTimeAttributeConverter;
        this.passwordEncoder = passwordEncoder;
    }

    public void addUserWithDefaultRole(UserDto userDto) {
        UserRole defaultRole = userRoleRepository.findByRole(DEFAULT_ROLE);
        userDto.getRoles().add(defaultRole);
        userDto.getUserDetails().setJoinDate(LocalDate.now());
        String passwordHash = passwordEncoder.encode(userDto.getPassword());
        userDto.setPassword(passwordHash);
        User user = modelMapper.map(userDto, User.class);
        userRepository.save(user);
    }

    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();

        return users.stream()
                .map(u -> modelMapper.map(u, UserDto.class))
                .collect(Collectors.toList());
    }

    public void blockUser(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User not found"));

        LocalDateTime lockDateTime = LocalDateTime.now();
        user.setLockDate(localDateTimeAttributeConverter.convertToDatabaseColumn(lockDateTime));
        user.setUnlockDate(localDateTimeAttributeConverter.convertToDatabaseColumn(lockDateTime.plusMinutes(ACCOUNT_BLOCK_TIME_IN_MINUTES)));
        userRepository.save(user);
    }

    public void unblockUser(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User not found"));
        user.setLockDate(null);
        user.setUnlockDate(null);
        userRepository.save(user);
    }

    @Scheduled(fixedRate = 10000)
    public void checkUserStatus() {
        List<User> users = userRepository.findAllToUnlock();

        for (User user : users) {
            user.setLockDate(null);
            user.setUnlockDate(null);
            userRepository.save(user);
        }
    }
}
