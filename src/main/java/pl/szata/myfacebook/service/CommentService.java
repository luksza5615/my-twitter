package pl.szata.myfacebook.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.szata.myfacebook.entity.Comment;
import pl.szata.myfacebook.entity.User;
import pl.szata.myfacebook.model.CommentDto;
import pl.szata.myfacebook.repository.CommentRepository;
import pl.szata.myfacebook.repository.UserRepository;
import pl.szata.myfacebook.util.LocalDateTimeAttributeConverter;
import pl.szata.myfacebook.util.UserChecker;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
public class CommentService {
    private ModelMapper modelMapper;
    private CommentRepository commentRepository;
    private LocalDateTimeAttributeConverter localDateTimeAttributeConverter;
    private UserRepository userRepository;
    private UserChecker userChecker;

    @Autowired
    public CommentService(ModelMapper modelMapper,
                          CommentRepository commentRepository,
                          LocalDateTimeAttributeConverter localDateTimeAttributeConverter,
                          UserRepository userRepository,
                          UserChecker userChecker) {
        this.modelMapper = modelMapper;
        this.commentRepository = commentRepository;
        this.localDateTimeAttributeConverter = localDateTimeAttributeConverter;
        this.userChecker = userChecker;
        this.userRepository = userRepository;
    }

    public void addNewComment(CommentDto commentDto, Long id) {
        User user = userRepository.findByUsername(userChecker.checkCurrentUserByUsername());
        commentDto.setCreateDate(localDateTimeAttributeConverter.convertToDatabaseColumn(LocalDateTime.now()));
        commentDto.setPostId(id);
        commentDto.setUser(user);
        Comment comment = modelMapper.map(commentDto, Comment.class);
        commentRepository.save(comment);
    }

    @Transactional
    public void deleteComment(Long id) {
        Comment comment = commentRepository.findCommentById(id)
                .orElseThrow(() -> new RuntimeException("Post not found"));

        User loggedUser = userRepository.findByUsername(userChecker.checkCurrentUserByUsername());

        boolean isLoggedUserAdmin = userChecker.isLoggedUserAdmin(loggedUser);

        if ((!comment.getUser().getId().equals(loggedUser.getId())) && !isLoggedUserAdmin) {
            throw new RuntimeException("Deleting not yours comments not possible");
        }

        comment.setDeleteDate(localDateTimeAttributeConverter.convertToDatabaseColumn(LocalDateTime.now()));
    }

    public void modifyComment(CommentDto commentDto, Long id) {
        Comment comment = commentRepository
                .findCommentById(id)
                .orElseThrow(() -> new RuntimeException("Comment not found"));

        User loggedUser = userRepository.findByUsername(userChecker.checkCurrentUserByUsername());

        boolean isLoggedUserAdmin = userChecker.isLoggedUserAdmin(loggedUser);

        if ((!comment.getUser().getId().equals(loggedUser.getId())) && !isLoggedUserAdmin) {
            throw new RuntimeException("Modyfing not yours comments not possible");
        }

        comment.setText(commentDto.getText());
        comment.setModifyDate(localDateTimeAttributeConverter.convertToDatabaseColumn(LocalDateTime.now()));
        commentRepository.save(comment);
    }
}
