package pl.szata.myfacebook.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.szata.myfacebook.model.CommentDto;
import pl.szata.myfacebook.model.PostDto;
import pl.szata.myfacebook.service.CommentService;
import pl.szata.myfacebook.service.PostService;

@Controller
public class CommentController {

    private final CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping("/addcomment/{postId}")
    public String addNewComment(@ModelAttribute CommentDto commentDto, @PathVariable("postId") Long postId) {
        commentService.addNewComment(commentDto, postId);
        return "redirect:../index";
    }

    @RequestMapping(value = "/changecomment/{id}", method = RequestMethod.POST, params = "action=delete")
    public String deleteComment(@PathVariable Long id) {
        commentService.deleteComment(id);
        return "redirect:../index";
    }

    @RequestMapping(value = "/changecomment/{id}", method = RequestMethod.POST, params = "action=modify")
    public String editComment(@ModelAttribute("comment") CommentDto commentDto, @PathVariable Long id, Model model) {
        model.addAttribute("commentToChange", id);
        return "editComment";
    }

    @RequestMapping(value = "/changecomment/{id}", method = {RequestMethod.PUT, RequestMethod.GET})
    public String editComment(@ModelAttribute("comment") CommentDto commentDto, @PathVariable Long id) {
        commentService.modifyComment(commentDto, id);
        return "redirect:../index";
    }
}
