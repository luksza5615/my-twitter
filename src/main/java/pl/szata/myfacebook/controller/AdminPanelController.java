package pl.szata.myfacebook.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.szata.myfacebook.model.UserDto;
import pl.szata.myfacebook.service.UserService;

import java.time.LocalDateTime;
import java.util.List;

@Controller
public class AdminPanelController {

    private final UserService userService;

    public AdminPanelController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/adminpanel")
    public String showAdminPanelAndUsers(Model model) {
        List<UserDto> users = userService.getAllUsers();
        model.addAttribute("allUsers", users);
        model.addAttribute("localDateTime", LocalDateTime.now()); //dodane testowo
        return "adminPanel";
    }
}
