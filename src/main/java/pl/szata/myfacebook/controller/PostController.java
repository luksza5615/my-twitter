package pl.szata.myfacebook.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.szata.myfacebook.model.CommentDto;
import pl.szata.myfacebook.model.PostDto;
import pl.szata.myfacebook.service.PostService;

import java.util.List;

@Controller
public class PostController {

    private final PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @PostMapping("/addpost")
    public String addPost(@ModelAttribute PostDto postDto, Model model) {
        List<PostDto> posts = postService.getAllPosts();
        model.addAttribute("postToInsert", new PostDto());
        postService.addNewPost(postDto);
        return "redirect:index";
    }

    @GetMapping("/index")
    public void showAllPosts(Model model) {
        List<PostDto> posts = postService.getAllPosts();
        model.addAttribute("postToInsert", new PostDto());
        model.addAttribute("allPosts", posts);
        model.addAttribute("commentToInsert", new CommentDto());
    }

    @RequestMapping(value = "/changepost/{id}", method = RequestMethod.POST, params = "action=delete")
    public String deletePost(@PathVariable Long id) {
        postService.deletePost(id);
        return "redirect:../index";
    }

    @RequestMapping(value = "/changepost/{id}", method = RequestMethod.POST, params = "action=modify")
    public String modifyPost(@ModelAttribute("post") PostDto postDto, @PathVariable("id") String id) {
        return "editPost";
    }

    @RequestMapping(value = "/changepost", method = {RequestMethod.PUT, RequestMethod.GET})
    public String modifyPost(@ModelAttribute("post") PostDto postDto) {
        postService.modifyPost(postDto);
        return "redirect:index";
    }

    @RequestMapping(value = "/changepost/{id}", method = RequestMethod.POST, params = "action=addcomment")
    public String addComment(@ModelAttribute("post") PostDto postDto, Model model) {
        model.addAttribute("commentToInsert", new CommentDto());
        return "addComment";
    }
}
