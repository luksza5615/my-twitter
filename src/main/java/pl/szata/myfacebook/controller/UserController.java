package pl.szata.myfacebook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import pl.szata.myfacebook.model.UserDto;
import pl.szata.myfacebook.service.UserService;

import javax.validation.Valid;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/registeruser")
    public String registerUser(Model model) {
        model.addAttribute("userToInsert", new UserDto());
        return "registerForm";
    }

    @PostMapping("/registeruser")
    public String registerUser(@ModelAttribute("userToInsert") @Valid UserDto userDto,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "registerForm";
        } else {
            userService.addUserWithDefaultRole(userDto);
            return "registerSuccess";
        }
    }

    @RequestMapping(value = "/changestatus/{id}", method = RequestMethod.POST, params = "action=block")
    public String blockUser(@PathVariable("id") Long id, Model model) {
        userService.blockUser(id);
        return "redirect:../adminpanel";
    }

    @RequestMapping(value = "/changestatus/{id}", method = RequestMethod.POST, params = "action=unblock")
    public String unblockUser(@PathVariable("id") Long id, Model model) {
        userService.unblockUser(id);
        return "redirect:../adminpanel";
    }
}
