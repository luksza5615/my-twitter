## Description

MyFacebook is a final project made during Java course. It mirrors basic facebook view and features and contains:
* users registration
* logging in
* adding/deleting/modifying logged user's posts
* adding/deleting/modifying logged user's comments
* administrator panel – users’ review, blocking users 
* user and administrator role
* password encoding


## Stack
* Java
* Spring Boot
* Spring (Core, MVC, Security, Data)
* Thymeleaf
* HTML, CSS
* MySql
* Maven

![](images/view.png)

![](images/login.png)
